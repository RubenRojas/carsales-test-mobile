package cl.ruben.carsalestechtest.presentation.common

import java.text.SimpleDateFormat

val apiDateFormat by lazy { SimpleDateFormat("yyyy-MM-dd") }
const val apiTries = 3

