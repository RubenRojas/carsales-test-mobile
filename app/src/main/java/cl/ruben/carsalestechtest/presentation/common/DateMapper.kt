package cl.ruben.carsalestechtest.presentation.common


import java.util.*

fun Date.toDisplay() : String{
    val currentDate = apiDateFormat.format(this)
    val element = currentDate.split("-")
    return "${element[2]} de ${element[1]} del ${element[0]}"

}