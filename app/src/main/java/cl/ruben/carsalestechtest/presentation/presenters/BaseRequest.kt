package cl.ruben.carsalestechtest.presentation.presenters

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class BaseRequest {
    fun toMap(): Map<String, String>{
        val gson = Gson()
        val jsonData: String = gson.toJson(this)
        return gson.fromJson(jsonData, object : TypeToken<Map<String, String>>() {}.type)
    }
}