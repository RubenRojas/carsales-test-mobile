package cl.ruben.carsalestechtest.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import cl.ruben.carsalestechtest.common.Response
import cl.ruben.carsalestechtest.data.models.mappers.toModel
import cl.ruben.carsalestechtest.data.models.mappers.toPresenter
import cl.ruben.carsalestechtest.data.repository.StatisticsRepository
import cl.ruben.carsalestechtest.data.repository.statisticsRepository
import cl.ruben.carsalestechtest.presentation.presenters.StatisticsRequestPresenter
import kotlinx.coroutines.Dispatchers

val statisticsViewModel : StatisticsViewModel by lazy { StatisticsViewModel() }

class StatisticsViewModel(private val repo : StatisticsRepository = statisticsRepository) : ViewModel() {
    fun getStatisticsByDate(request: StatisticsRequestPresenter) = liveData(Dispatchers.IO){
        emit(Response.loading(data = null))
        try{
            emit(Response.success(repo.getStatisticsByDate(request.toModel().toMap()).toPresenter()))
        }
        catch (e: Exception){
            emit(Response.error(data = null, message = e.message ?: common.defaultErrorMessage))
        }
    }
}