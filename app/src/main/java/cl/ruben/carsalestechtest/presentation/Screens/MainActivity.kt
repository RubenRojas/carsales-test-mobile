package cl.ruben.carsalestechtest.presentation.Screens


import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import cl.ruben.carsalestechtest.R
import cl.ruben.carsalestechtest.common.ResponseStatus
import cl.ruben.carsalestechtest.databinding.ActivityMainBinding
import cl.ruben.carsalestechtest.presentation.common.apiDateFormat

import cl.ruben.carsalestechtest.presentation.common.apiTries
import cl.ruben.carsalestechtest.presentation.common.toDisplay
import cl.ruben.carsalestechtest.presentation.presenters.StatisticsPresenter
import cl.ruben.carsalestechtest.presentation.presenters.StatisticsRequestPresenter
import cl.ruben.carsalestechtest.presentation.viewmodels.statisticsViewModel

import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.util.*


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var date : Date
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, -1)

        date  = Date(cal.timeInMillis)
        getStatistics(apiTries, date)

        val datePicker =
            MaterialDatePicker.Builder.datePicker()
                    .setTitleText(R.string.dialog_select_date_title)
                .build()

        binding.dateSelector.setOnClickListener{
            datePicker.show(supportFragmentManager, "datePicker")
        }

        datePicker.addOnPositiveButtonClickListener {
            date  = Date(it)
            date.minutes = (date.minutes + date.timezoneOffset)
            getStatistics(apiTries, date)
        }
    }


    private fun displayStatistics(presenter: StatisticsPresenter) {
        binding.confirmedCases.text = presenter.confirmed.toString()
        binding.deaths.text = presenter.deaths.toString()
        binding.currentDate.text = date.toDisplay()
    }

    private fun getStatistics(tries: Int, date: Date) {
        if (tries > 0) {
            statisticsViewModel.getStatisticsByDate(
                StatisticsRequestPresenter(
                    apiDateFormat.format(date)
                )
            )
                .observe(this, { (status, response, error) ->
                    when (status) {
                        ResponseStatus.ERROR -> {
                            if (error != null) {
                                Log.d("Develop", error)
                            }
                            getStatistics(tries - 1, date)
                        }
                        ResponseStatus.SUCCESS -> {
                            LoadingScreen.hideLoading()
                            if (response != null) {
                                displayStatistics(response)
                            }

                        }
                        ResponseStatus.LOADING ->{
                            LoadingScreen.displayLoadingWithText(this,false)
                        }
                    }


                })
        } else {
            LoadingScreen.hideLoading()
            MaterialAlertDialogBuilder(applicationContext)
                .setTitle(resources.getString(R.string.error_dialog_title))
                .setMessage(resources.getString(R.string.error_dialog_message))
                .setPositiveButton(resources.getString(R.string.dialog_ok_button_label)) { dialog, which ->
                }
                .show()
        }
    }


}