package cl.ruben.carsalestechtest.presentation.presenters

data class StatisticsPresenter (
    val confirmed : Long,
    val deaths : Long,
        )