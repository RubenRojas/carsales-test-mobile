package cl.ruben.carsalestechtest.common

import cl.ruben.carsalestechtest.data.api.StatisticsApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val builder : RetrofitBuilder by lazy { RetrofitBuilder() }

val statisticsApi : StatisticsApi = builder.getRetrofit().create(StatisticsApi::class.java)

class RetrofitBuilder {
    fun getRetrofit(): Retrofit {

        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient.Builder = OkHttpClient.Builder()

        client.addInterceptor(logging)

        return Retrofit.Builder()
            .baseUrl(Endpoints.baseUrl)
            .client(client.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}