package cl.ruben.carsalestechtest.common

enum class ResponseStatus {
    SUCCESS,
    ERROR,
    LOADING,
}