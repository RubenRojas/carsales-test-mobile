package cl.ruben.carsalestechtest.common

class Endpoints {

    companion object {
        const val baseUrl = "https://covid-19-statistics.p.rapidapi.com/"
        const val covidStatisticsApi = "reports/total"
    }
}