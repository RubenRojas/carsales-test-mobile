package cl.ruben.carsalestechtest.data.source.remote

import cl.ruben.carsalestechtest.common.statisticsApi
import cl.ruben.carsalestechtest.data.api.StatisticsApi
import cl.ruben.carsalestechtest.data.models.StatisticsApiResponse
import retrofit2.await

val statisticsSource : StatisticsSource by lazy { StatisticsSource() }

class StatisticsSource(private val api : StatisticsApi = statisticsApi) {
    suspend fun getStatistics(request : Map<String, String>?) : StatisticsApiResponse{

        // todo: Hide api key
        return api.getStatisticsByDate(apiKey = "96afa298cbmsh913f910f914494cp110c39jsn01a32d68445e", params = request).await()
    }
}