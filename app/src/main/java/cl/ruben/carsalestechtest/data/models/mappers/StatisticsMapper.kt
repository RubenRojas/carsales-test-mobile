package cl.ruben.carsalestechtest.data.models.mappers

import cl.ruben.carsalestechtest.data.models.StatisticsApiResponse
import cl.ruben.carsalestechtest.data.models.requests.StatisticsRequestModel
import cl.ruben.carsalestechtest.presentation.presenters.StatisticsPresenter
import cl.ruben.carsalestechtest.presentation.presenters.StatisticsRequestPresenter

fun StatisticsRequestPresenter.toModel() : StatisticsRequestModel{
    return StatisticsRequestModel(
        date
    )
}

fun StatisticsApiResponse.toPresenter() : StatisticsPresenter{
    return StatisticsPresenter(
        this.data.confirmed,
        this.data.deaths
    )
}