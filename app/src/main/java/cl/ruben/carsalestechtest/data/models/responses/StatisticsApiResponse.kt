package cl.ruben.carsalestechtest.data.models

data class StatisticsApiResponse (
    val data: Data
)

data class Data (
    val date: String,
    val lastUpdate: String,
    val confirmed: Long,
    val confirmedDiff: Long,
    val deaths: Long,
    val deathsDiff: Long,
    val recovered: Long,
    val recoveredDiff: Long,
    val active: Long,
    val activeDiff: Long,
    val fatalityRate: Double
)