package cl.ruben.carsalestechtest.data.models.requests

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

 class StatisticsRequestModel(
    val date: String
){
    fun toMap(): Map<String, String> {
        val gson = Gson()
        val jsonData: String = gson.toJson(this)
        return gson.fromJson(
            jsonData,
            object : TypeToken<Map<String, String>>() {}.type
        )
    }
}