package cl.ruben.carsalestechtest.data.repository

import cl.ruben.carsalestechtest.data.models.StatisticsApiResponse
import cl.ruben.carsalestechtest.data.source.remote.StatisticsSource
import cl.ruben.carsalestechtest.data.source.remote.statisticsSource

val statisticsRepository : StatisticsRepository by lazy { StatisticsRepository() }

class StatisticsRepository (private val source : StatisticsSource = statisticsSource){

    suspend fun getStatisticsByDate(request : Map<String, String>?) : StatisticsApiResponse{
        return source.getStatistics(request)
    }
}