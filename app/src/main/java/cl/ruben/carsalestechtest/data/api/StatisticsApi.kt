package cl.ruben.carsalestechtest.data.api

import cl.ruben.carsalestechtest.common.Endpoints
import cl.ruben.carsalestechtest.data.models.StatisticsApiResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.QueryMap

interface StatisticsApi {


    @GET(Endpoints.covidStatisticsApi)
    fun getStatisticsByDate(
        @Header("X-RapidAPI-Key") apiKey: String,
        @QueryMap params: Map<String, String>?
    ): Call<StatisticsApiResponse>
}
